import os
import json
import xml.etree.ElementTree as et

import pytest


@pytest.fixture
def four_articles_paths():
    xml = os.path.join("tests", "sample_xml", "four_articles.xml")
    results = os.path.join("tests", "sample_xml", "four_articles_results.json")
    return xml, results


@pytest.fixture
def five_articles_paths():
    xml = os.path.join("tests", "sample_xml", "five_articles_with_foreign_grants.xml")
    results = os.path.join(
        "tests", "sample_xml", "five_articles_with_foreign_grants_results.json"
    )
    return xml, results


@pytest.fixture
def sample_xml(four_articles_paths):
    xml, results = four_articles_paths
    with open(xml, "r") as f:
        et_xml = et.fromstring(f.read())
    with open(results, "r") as f:
        json_res = json.load(f)
    return et_xml, json_res


@pytest.fixture
def sample_xml_grants(five_articles_paths):
    xml, results = five_articles_paths
    with open(xml, "r") as f:
        et_xml = et.fromstring(f.read())
    with open(results, "r") as f:
        json_res = json.load(f)
    return et_xml, json_res
