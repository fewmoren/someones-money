from pubmedapi import parse


def test_parse_medline_xml(sample_xml):
    xml, expected = sample_xml
    res = parse.parse_pubmed_xml(xml)
    assert res == expected


def test_parse_medline_xml_funny_grants(sample_xml_grants):
    xml, expected = sample_xml_grants
    res = parse.parse_pubmed_xml(xml)
    assert res == expected
