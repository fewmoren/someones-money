def get_pmid_xml(pubmed_entry):
    # relative to the medline element/node
    pmid = pubmed_entry.find("./MedlineCitation/PMID")
    return pmid.text


def get_journal_name_xml(pubmed_element):
    jn = pubmed_element.find("./MedlineCitation/Article/Journal/Title")
    return jn.text.lower().strip()


def get_published_date_xml(pubmed_element):
    pubmed_date_element = pubmed_element.find(
        "./PubmedData/History/PubMedPubDate[@PubStatus='pubmed']"
    )
    published_mo = int(pubmed_date_element.find("./Month").text)
    published_year = pubmed_date_element.find("./Year").text
    return f"{published_mo:02}/{published_year}"


def is_journal_article(pubmed_entry):
    # only articles with the article MeSH tag accepted
    attrs = [
        entry.attrib["UI"]
        for entry in pubmed_entry.findall(
            "./MedlineCitation/Article/PublicationTypeList/PublicationType"
        )
    ]
    if not "D016428" in attrs:
        return False
    return True


def get_publication_funding_type_xml(pubmed_entry):
    # see MeSH tags: https://meshb-prev.nlm.nih.gov/record/ui?ui=D057689
    out = {
        "USGovernmentFunding": False,
        "USGovernmentFundingExtramural": False,
        "USGovernmentFundingIntramural": False,
        "NonUSGovernmentFunding": False,
        "IsRetracted": False,
    }
    attribs = [
        entry.attrib["UI"]
        for entry in pubmed_entry.findall(
            "./MedlineCitation/Article/PublicationTypeList/PublicationType"
        )
    ]
    if "D057689" in attribs:
        out["USGovernmentFunding"] = True
    if "D052061" in attribs:
        out["USGovernmentFundingExtramural"] = True
        out["USGovernmentFunding"] = True
    if "D052060" in attribs:
        out["USGovernmentFundingIntramural"] = True
        out["USGovernmentFunding"] = True
    if "D013485" in attribs:
        out["NonUSGovernmentFunding"] = True
    if "D016441" in attribs:
        out["IsRetracted"] = True
    return out


def get_grant_list_xml(pubmed_element):
    all_grants = []
    for grants in pubmed_element.findall("./MedlineCitation/Article/GrantList/Grant"):
        grant_entry = {}
        try:
            grant_entry["id"] = grants.find("./GrantID").text
        except:
            grant_entry["id"] = "Not found"
        try:
            grant_entry["IC"] = grants.find("./Acronym").text
        except:
            grant_entry["IC"] = "Not found"
        try:
            grant_entry["agency"] = grants.find("./Agency").text
        except:
            grant_entry["agency"] = "Not found"
        try:
            grant_entry["country"] = grants.find("./Country").text
        except:
            grant_entry["country"] = "Not found"
        all_grants.append(grant_entry)
    return all_grants


def parse_pubmed_xml_DEBUG(xml_root):
    extracted_data = []
    for pubmed_entry in xml_root.findall("./"):
        entry = {}
        entry["pmid"] = get_pmid_xml(pubmed_entry)
        if not is_journal_article(pubmed_entry):
            print(f"[INFO] PMID: '{entry['pmid']}' Not an article, continuing...")
            continue
        try:
            pub_and_funding = get_publication_funding_type_xml(pubmed_entry)
        except:
            print(f"[WANRING] funding type error PMID: '{entry['pmid']}'")
        try:
            entry["journal_name"] = get_journal_name_xml(pubmed_entry)
        except:
            print(f"[WANRING] journal name error PMID: '{entry['pmid']}'")
        try:
            entry["pubmed_published_date"] = get_published_date_xml(pubmed_entry)
        except:
            print(f"[WANRING] pubmed published date error PMID: '{entry['pmid']}'")
        try:
            entry["grant_list"] = get_grant_list_xml(pubmed_entry)
        except:
            print(f"[WARNING] grant list error PMID: '{entry['pmid']}'")
        try:
            extracted_data.append({**entry, **pub_and_funding})
        except Exception as e:
            print(f"[WARNING] Exception: '{e}' with PMID: '{entry['pmid']}'")
            continue
    return extracted_data


def parse_pubmed_xml(xml_root):
    extracted_data = []
    us_extramural_grant_types = [
        "R01",
        "R03",
        "R13",
        "R15",
        "R21",
        "R34",
        "R56",
        "U01",
        "K99/R00",
        "K99",
        "R00",
        "T31",
        "T32",
        "F30",
        "F99/K00",
        "F99",
        "K00",
        "F31",
        "F32",
    ]
    for pubmed_entry in xml_root.findall("./"):
        entry = {}
        entry["pmid"] = get_pmid_xml(pubmed_entry)
        attrs = pubmed_entry.find("MedlineCitation").attrib
        entry["owner"] = attrs["Owner"]
        # note only Status MEDLINE are completed citations
        # https://www.nlm.nih.gov/bsd/licensee/elements_descriptions.html#status_value
        entry["medline_status"] = attrs["Status"]
        if not is_journal_article(pubmed_entry):
            print(f"[INFO] PMID: '{entry['pmid']}' Not an article, continuing...")
            continue
        try:
            pub_and_funding = get_publication_funding_type_xml(pubmed_entry)
            entry["journal_name"] = get_journal_name_xml(pubmed_entry)
            entry["pubmed_published_date"] = get_published_date_xml(pubmed_entry)
            entry["grant_list"] = get_grant_list_xml(pubmed_entry)
            for grant in entry["grant_list"]:
                if grant.get("country") == "United States":
                    pub_and_funding["USGovernmentFunding"] = True
                grant_id = grant.get("id")
                for funding_type in us_extramural_grant_types:
                    if funding_type in grant_id:
                        pub_and_funding["USGovernmentFundingExtramural"] = True
                        pub_and_funding["USGovernmentFunding"] = True
            extracted_data.append({**entry, **pub_and_funding})

        except Exception as e:
            print(f"[WARNING] Exception: '{e}' with PMID: '{entry['pmid']}'")
            continue
    return extracted_data
