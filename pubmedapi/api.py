import json
from pprint import pprint
import xml.etree.ElementTree as et

import requests
import re

from pubmedapi import parse

# Medline fields of interest are GR and PT, often there are many
def fmt_journal(item):
    item += "[Journal]"
    return item.replace(" ", "+")


def _call_for_items(url, pmids=[], retstart=0):
    new_start = f"&retstart={retstart}"
    url = re.sub("&retstart=\d+", new_start, url)
    req = requests.get(url)
    try:
        pmids += req.json()["esearchresult"]["idlist"]
    except:
        raise AssertionError(
            f"{req.json()['esearchresult'].keys()}\n\n{req}\n\n{req.json()['esearchresult']['ERROR']}\n\n{req.url}"
        )
    return req.url, pmids, req.json()


def get_pmids(journals: list, start_date: str, stop_date: str):
    # Date in YYYY/MM/DD format. Searches Mesh Date (MHDA)
    # all journal official names: https://ftp.ncbi.nih.gov/pubmed/J_Medline.txt
    journals_formatted = [fmt_journal(i) for i in journals]
    formatted_journals_string = "(" + "+OR+".join(journals_formatted) + ")"

    base = f"https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed&term={formatted_journals_string}+AND+journal+article[pt]+NOT+comment[pt]+NOT+review[pt]&retmode=json&mindate={start_date}&maxdate={stop_date}&retmax=100000&retstart=0&datetype=mhda"
    url, pmids, req_json = _call_for_items(base)
    target_length = int(req_json["esearchresult"]["count"])
    print(f"Total count: {target_length}")
    length_current = len(pmids)
    # get pmid's in batches if request is > 100K (max api returns)
    while length_current < target_length:
        url, pmids, req_json = _call_for_items(url, pmids, length_current)
        length_current = len(pmids)
        print(length_current)
    return pmids, req_json


def get_batch_xml(pmid: list):
    """takes a list of pubmed ID's and returns an XML document with the data"""
    base = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?"
    parameters = {"db": "pubmed", "rettype": "XML", "id": pmid}
    print(f"[INFO] Requesting {len(pmid)} PMID's...")
    results = requests.get(base, params=parameters)
    return results.text, results


def debug_return_entries(xml_string):
    xml = et.fromstring(xml_string)
    return [i for i in xml.findall("./")]


# for testing output

# "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed&term=(nat+neurosci[Journal]+AND+Journal+Article[pt]+NOT+comment[pt]+NOT+retraction+of+publication[pt])&rettype=json&mindate=2022/01/01&maxdate=2022/01/22&datetype=mhda"

if __name__ == "__main__":
    journals = [
        "Nature neuroscience",
        "Neuron",
        "J Neurosci",
        "Eneuro",
        "Glia",
        "Nature",
        "Science",
        "Cell",
        "elife",
        "Plos Biology",
        "Nat Commun",
        "Cell Rep",
    ]
    pmid_list, response = get_pmids(journals, 2016, 2020)
    all_data = []
    # pmid_subset = pmid_list[:400]
    step_size = 200
    while pmid_list:  # pmid_subset:
        data, _ = get_batch_xml(pmid_list[:step_size])
        root = et.fromstring(data)
        results = parse.parse_pubmed_xml(root)
        all_data += results
        del pmid_list[:step_size]
        # del pmid_subset[:step_size]
    with open("results.json", "w") as f:
        json.dump(all_data, f)
