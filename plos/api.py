# plos one api
# search fields http://api.plos.org/solr/search-fields/
import json

import requests


def plos_query(year, journal):
    plos = f"http://api.plos.org/search?q=publication_date:[{year}-01-01T00:00:00Z%20TO%20{year}-12-31T23:59:59Z]&fq=doc_type:full&fq=journal:%22{journal}%22&wt=json&fl=id,publication_date,title,journal,article_type"
    return requests.get(plos)


def parse_records(response):
    try:
        as_json = response.json()
        return as_json["response"]["numFound"], response
    except Exception as e:
        print(f"[ERROR] Couldn't convert to JSON")
        return "", response


if __name__ == "__main__":

    YEARS = range(2017, 2022)
    JOURNALS = ["PLoS One", "PLoS Biology"]

    data = []
    for year in YEARS:
        for journal in JOURNALS:
            print(f"Requesting year: {year}, journal: {journal}")
            res = plos_query(year, journal)
            n, _ = parse_records(res)
            data.append(
                {
                    "year": year,
                    "open_access": n,
                    "subscription": 0,
                    "journal": journal,
                    "journal_type": "open",
                }
            )
    with open("plos_2017-2021.json", "w") as f:
        json.dump(data, f, indent=2)
