# scrape article data from nature
import json
import requests

import lxml.html as html


def _check_oa(article_item):
    # careful, lxml deprication:
    if article_item.find(".//span[@itemprop='openAccess']"):
        return True
    return False


def article_metadata(article_item):
    link_item = article_item.find(".//a")
    href = link_item.attrib["href"]
    title = link_item.text
    link = "https://nature.com" + href
    a_type = article_item.find(".//span/").text
    date = article_item.find(".//time").attrib["datetime"]
    oa = _check_oa(article_item)
    return {
        "title": title,
        "link": link,
        "article_type": a_type,
        "date": date,
        "year": date.split("-")[0],
        "isOpenAccess": oa,
    }


def get_article_metadata(root):
    metadata = []
    articles = root.findall(".//article")
    journal = root.find(".//span[@class='c-meta__item']").text
    journal_fmt = journal.replace("(", "").strip()
    for article in articles:
        article_meta = article_metadata(article)
        article_meta["journal"] = journal_fmt
        metadata.append(article_meta)
    return metadata


def nature_request(base, year, page=1):
    args = {
        "year": year,
        "page": page,
        "searchType": "journalSearch",
        "sort": "PubDate",
    }
    return requests.get(base, params=args)


def iter_articles(base, year):
    per_year = []
    first_call = nature_request(base, year=year, page=1)
    root = html.fromstring(first_call.text)
    max_pages = root.find(".//li[@class='c-pagination__item'][last()-1]").attrib[
        "data-page"
    ]
    print(f"[INFO] Requesting page 1 of {max_pages}")
    per_year += get_article_metadata(root)
    for n in range(2, int(max_pages) + 1):
        print(f"[INFO] Requesting page {n} of {max_pages}")
        page = nature_request(base, year, page=n)
        root = html.fromstring(page.text)
        per_year += get_article_metadata(root)
    return per_year


def count_articles(metadata):
    oa = 0
    subscription = 0
    for article in metadata:
        if article["isOpenAccess"]:
            oa += 1
        else:
            subscription += 1
    return {article["year"]: {"openAccess": oa, "subscription": subscription}}


# res = nature_request(natureneuro, year=2021, page=1)
# root = html.fromstring(res.text)
# metadata = get_article_metadata(root)

if __name__ == "__main__":
    NATURE = "https://www.nature.com/nature/research-articles"
    NATURENEURO = "https://www.nature.com/neuro/research-articles"
    YEAR_RANGE = range(2017, 2022)
    nature_metadata = []
    natureneuro_metadata = []
    print("[INFO] Getting Nature Neuroscience...")
    for year in YEAR_RANGE:
        print(f"[INFO] Getting year {year}")
        articles = iter_articles(NATURENEURO, year)
        natureneuro_metadata += articles
    with open("natureneuroscience_2017-2021.json", "w") as f:
        json.dump(natureneuro_metadata, f, indent=2)
    print("[INFO] Getting Nature...")
    for year in YEAR_RANGE:
        print(f"[INFO] Getting year {year}")
        articles = iter_articles(NATURE, year)
        nature_metadata += articles
    with open("nature_2017-2021.json", "w") as f:
        json.dump(nature_metadata, f, indent=2)
