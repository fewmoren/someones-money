# Someone Else's Money Journal Dataset Builder and Analysis Code

Functions to gather and analyze data used for the article [Publishing with other people's money](https://n-of-2.com/publishing-with-other-peoples-money/).

These scripts require Python 3.9 or greater, and the following libraries:
- `lxml` for parsing HTML
- `requests` to make web requests

## pubmedapi

Functions to gather publication data using the pubmed entrez API. 

**NOTE**: 
The functions in `pubmedapi/` are not used for this project, so we removed the `setup.py`. `pubmedapi` is included for posterity, and will likely be updated as a library of its own in the future.


### About the Entrez the APIs

Entrez is a set applications you communicate with over API's to query the National Center for Biotechnology Information (NCBI) databases.
The applications include:
- `EInfo` 
- `ESearch`
- `EPost`
- `ESummary`
- `EFetch`

For more information on these tools, see the [NCBI documentation](https://www.ncbi.nlm.nih.gov/books/NBK25497/)

### Manually calling the entrez API:

The base API call to get a article's pubmed/medline data as XML (in this case PMID:34266896):

``` bash
https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&rettype=XML&id=34266896
```

### Notes and Instructions Relating to the pubmed API's

- [MEDLINE terms](https://www.nlm.nih.gov/bsd/mms/medlineelements.html)
- [MeSH subject headings](https://meshb.nlm.nih.gov/treeView)
  - [MEDLINE attributes explained](https://www.nlm.nih.gov/bsd/licensee/elements_descriptions.html#grantlist)
- [Cost of publishing Nature](https://www.nature.com/articles/495426a)
- [NIH Reporter API](https://api.reporter.nih.gov/#/publication-output-for-search-endpoint)


## plos

Gathers data from the PLoS API for PLoS Biology and PLoS One.

[API Documentation](http://api.plos.org/solr/search-fields/). 

Running this script with:

```python3 
python3 plos/api.py
```

will build the file currently stored in `data/plos_2017-2021.json`

## nature

Gathers data by requesting and parsing article lists on the nature website for *Nature*, *Nature Neuroscience*, and *Nature Communications*.

Running this script with:

```python3 
python3 nature/scrape.py
```

will build the file currently stored in `data/nature_2017-2021.json` and `data/natureneuro_2017-2021`

`nature_communications.csv` was gathered by manually selecting the appropriate year from the web page and recording the number of articles. Wayback machine archived links are provided in this file as citations. 

## Other datasets gathered and provided
- `elife_2017-2021.csv` was gathered manually using pubmed filters. URLs are provided in the dataset. 
- `walt-crawford-2015-2020-plus-edits.csv` is an edited version of a [dataset](https://figshare.com/articles/dataset/Gold_Open_Access_6_2015-2020/14787888) generously provided by [Walt Crawford](http://waltcrawford.name/).
- `cell_press_summary.csv` is a dataset manually gathered from Elsevier's Cell Press websites. Each entry has a Wayback Machine archive link as a citation. *Cell Reports* data comes from using pubmed URL searches, and the citations are provided in the document.

- `article-publishing-charge-elsevier.csv` and `jnlactive-elsevier.csv` are edited csv versions of the [APC price list](https://web.archive.org/web/20220226215552/https://www.elsevier.com/about/policies/pricing#APC) for Open Access and Hybrid Journals and the list of [Active Journals](https://web.archive.org/web/20220226215046/https://www.elsevier.com/solutions/sciencedirect/journals-books/journal-title-lists) provided by Elsevier. The script [datasets.R](https://gitlab.com/fewmoren/someones-money/-/blob/master/R/dataset.R#L71-91) merges the data and calculates the number of journals in each category. 
- `someones-money_dataset.csv` Dataset built by [dataset.R](https://gitlab.com/fewmoren/someones-money/-/blob/master/R/dataset.R#L69) by joining and summarizing the other datasets in `data/`. This is the primary dataset analyzed in [Publishing with other people's money](https://n-of-2.com/publishing-with-other-peoples-money/). 
# R Dataset builder and analysis

Analysis and data formatting was performed using `R`. 

R Version information:

```
R version 3.6.1 (2019-07-05) -- "Action of the Toes"
Copyright (C) 2019 The R Foundation for Statistical Computing
Platform: x86_64-apple-darwin15.6.0 (64-bit)
```

The files in `R/` will build the primary dataset and create the figures for the article. 
This file expects the working directory to be the root of this project. Note some files are missing, such as `paths.R` which contains a function that builds the path to the article. 
